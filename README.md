# Boilerplate 🧪 Flask + Docker

## General Idea
![Boilerplate Overview](https://cityguessr.fun/FD.png){width=33%}

The idea is a modern (in June 2023) and minimal Flask + Docker example which runs out of the box and can be extended any way or shape you like.
When you have built the docker image it will run as easily on your local computer as it will on a cloud provider like GCP, AWS and Azure.

This boilerplate also uses [Poetry](https://python-poetry.org/docs/#installation) which I personally think is the current best option for Python Package Management. 

## Quick Summary! (TL;DR)

* Clone this repo
* Change the occurences of `athena` to a name you like (name of file `athena.py` & on last line in `Dockerfile` & more) 
* Remove the existing `pyproject.toml` file, run `poetry init` and `poetry add flask gunicorn python-json-logger` on the root directory.
* Set environment variable `ENV=local` (depends on your OS/shell, but on PowerShell it's `$Env:ENV = "local"`)
* `poetry run python athena.py` will run the project locally 🤘🤘🤘
* Open http://localhost:8080/ in browser, see your awesome website
* Finally, write awesome code 🤓🤓🤓

**Read the full document for all the juicy details!**

## Shameless self-plug!

This boilerplate was created after I've started countless projects, each time refining it but still very similar.

My latest project [CityGuessr.fun](https://cityguessr.fun) use this boilerplate: A daily challenge webgame where you guess the city based on custom maps!

## Components

* `athena.py`: the main entrypoint for your webapp. Rename it to something appropriately. Expanding it, change it, (ab-)use it as you like!
  * When you change the file name of this file, you also need to change the reference to it on the last line of `Dockerfile`!
* `Dockerfile`: Docker build file. If you are not familiar maybe just google it?
* `/static` & `/templates`: Special folders used by Flask. Static is for static files like images, javascript, css, etc. Templates is for flask template files.
* `pyproject.toml`: The main kinda "definition" file for Poetry. Not recommended to manually edit, let Poetry deal with it.
* `.gitignore`: Files ignored by git, I've added some sensible defaults.

## Prerequisites
* [Python](https://www.python.org/downloads/) (with [Poetry](https://python-poetry.org/docs/#installation)) installed and working
* [Docker](https://docs.docker.com/get-docker/) ([Desktop?](https://docs.docker.com/desktop/)) installed and working

### Source code
For the sake of keeping things simple I have used "Athena" as a name for this fictive project, so anywhere you find "athena" replace it with your own project name. Especially important for the file (name and content of) `athena.py` of course!

### Poetry
This boilerplate includes a `pyproject.toml` which is the "definition file" for Poetry, but its only included for completeness. You should **delete this file, and then run:**
* `poetry init`
* `poetry add flask gunicorn python-json-logger`

## GOOD TO GO! (without Docker) 🤯🤯🤯
* `poetry shell -> python athena.py` or `poetry run python athena.py`
* open http://localhost:8080/ in browser, see your awesome website!

## GOOD TO GO! (with Docker) 🤯🤯🤯
* Build the docker image: `docker build . -t athena-app` 
* Run it: `docker run --env ENV=local -p 127.0.0.1:80:8080 athena-app`
* open http://localhost:8080/ in browser, see your awesome website!

(Remember: you need to build a new docker image each time you change your code!)